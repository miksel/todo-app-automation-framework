#!/usr/bin/Python

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import unittest, time

class ToDoApp(unittest.TestCase):

	global driver, wait 

	URL = "http://qa-test.avenuecode.com/tasks"
	USERNAME = "miksel9@gmail.com"
	PASSWORD = "avEnuEcodEqa"

	@classmethod
	def setUp(cls):
		cls.driver = webdriver.Chrome()
		cls.wait = WebDriverWait(cls.driver, 10)

	def testSignIn(self):
		self.driver.get(self.URL)
		assert "ToDo Rails and Angular" in self.driver.title

		self.driver.find_element_by_link_text("Sign In").click()
		assert "ToDo Rails and Angular" in self.driver.title

		self.driver.find_element_by_id("user_email").send_keys(self.USERNAME)
		self.driver.find_element_by_id("user_password").send_keys(self.PASSWORD)
		self.driver.find_element_by_name("commit").click()

		alert = self.driver.find_element_by_css_selector("div.alert.alert-info")
		if alert.text == "Signed in successfully.":
			return True
		else:
			return False

	@unittest.skip
	def testCreatingNewTask(self):

		self.testSignIn
