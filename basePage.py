#!/usr/bin/Python

from selenium import webdriver

class Page(object):
    """
    Base class that all page models can inherit from
    """
    def __init__(self,selenium_driver,base_url='http://qa-test.avenuecode.com/'):
        #We assume relative URLs start without a / in the beginning
        if base_url[-1] != '/': 
            base_url += '/' 
        self.base_url = base_url
        self.driver = selenium_driver
        #Visit and initialize xpaths for the appropriate page
        self.start() 
        
    def open_page(self,url):
        "Visit the page base_url + url"
        url = self.base_url + url
        self.driver.get(url)
 
    def get_element(self,xpath):
        "Return the DOM element of the xpath OR the 'None' object if the element is not found"
        return self.driver.find_element_by_xpath(xpath)

    def get_elements_by_tag_name(self, tag):
        return self.driver.find_elements_by_tag_name(tag)

    def click_element(self,xpath):
        "Click the button supplied"
        self.get_xpath(xpath).click()

    def get_title(self):
        return self.driver.title
 
    def wait(self,wait_seconds=5):
        " Performs wait for time provided"
        time.sleep(wait_seconds)