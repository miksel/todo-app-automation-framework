#!/usr/bin/Python

from basePage import Page

class LoginPage(Page):

	username = "miksel9@gmail.com"
	password = "avEnuEcodEqa"
	
	#locators 
    login_email = '//*[@id="user_email"]'
    login_password = '//*[@id="user_password"]'
    login_signin_button = '//*[@id="new_user"]/input'
    login_confirmation = '/html/body/div[1]/div[2]'

    def start(self):
        self.url = "users/sign_in"
        self.open_page(self.url) 
        # Assert Title of the Login Page and Login
        assert "ToDo Rails and Angular" in self.get_title()

    def login(self,username,password):
        "Login using credentials provided" 
        self.set_login_email(username)
        self.set_login_password(password)
        self.submit_login()
        if self.get_element(self.login_confirmation).text == "Signed in successfully.":
            return True
        else:
            return False

    def set_login_email(self,username):
        "Set the username on the login screen"
        self.get_element(self.login_email).send_keys(username)
 
    def set_login_password(self,password):
        "Set the password on the login screen"
        self.get_element(self.login_password).send_keys(password)
 
    def submit_login(self):
        "Submit the login form"
        self.click_element(self.login_signin_button)