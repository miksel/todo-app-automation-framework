#!/usr/bin/Python

from selenium import webdriver
from basePage import Page
from selenium.webdriver.common.by import By


class ToDoTaskPage(Page):

	#locators
	my_tasks_button = '/html/body/div[1]/div[1]/div/div[2]/ul[1]/li[2]/a'
	header = '/html/body/div[1]/h1'
	new_task_field = '//*[@id="new_task"]'
	add_task_button = "/html/body/div[1]/div[2]/div[1]/form/div[2]/span"
	task_list_table = "/html/body/div[1]/div[2]/div[2]/div/table"
	task_list_table_item = "tr"

	task_counter = 1
    new_task_string = "Sample task #" + str(task_counter)

    def start(self):
        self.url = "tasks/"
        self.open_page(self.url) 

        # Assert Title and Header of main task page
        assert "ToDo Rails and Angular" in self.get_title()

    def enter_new_task(self, new_task):
    	if new_task == None:
    		new_task = self.new_task_string

   		self.get_element(self.new_task_field).send_keys(self.new_task)

   	def get_header_text():
   		pass

   	def submit_new_task(self, new_task):
   		if len(self.new_task) >= 3 && len(self.new_task) <= 250:
   			self.click_element(self.get_element(self.add_task_button))
   			self.task_counter += 1
   			return True
   		else:
   			return False

   	def get_list_of_tasks(self):
   		return self.get_elements_by_tag_name(self.task_list_table_item)